$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/IMDBTopRatedMovies.feature");
formatter.feature({
  "name": "Verify The Top Rated Movies Page Of IMDB.",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Verify Top Rated Movies List Sort By Options and Order.",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Movie list should be displayed on basis of \"\u003csort by option\u003e\" in \"\u003cdefault order\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Click on Order changing button.",
  "keyword": "When "
});
formatter.step({
  "name": "Movie list should be displayed on basis of \"\u003csort by option\u003e\" in selected order",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "sort by option",
        "default order"
      ]
    },
    {
      "cells": [
        "Release Date",
        "Descending Order"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User Navigate To Top Rated Movies page from \"Top Rated Movies\" under \"Movies\" section.",
  "keyword": "Given "
});
formatter.match({
  "location": "com.automation.bdd.selenium.steps.CommonSteps.user_Navigate_To_Top_Rated_Movies_page_from_under_section(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Top Rated Movies page title should be \"Top Rated Movies\"",
  "keyword": "Then "
});
formatter.match({
  "location": "com.automation.bdd.selenium.steps.CommonSteps.top_rated_movies_page_title_should_be(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify Top Rated Movies List Sort By Options and Order.",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Movie list should be displayed on basis of \"Release Date\" in \"Descending Order\"",
  "keyword": "Then "
});
formatter.match({
  "location": "com.automation.bdd.selenium.steps.IMDBTopRatedMoviesTest.movie_list_should_be_displayed_on_basis_of_in(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Order changing button.",
  "keyword": "When "
});
formatter.match({
  "location": "com.automation.bdd.selenium.steps.IMDBTopRatedMoviesTest.click_on_Order_changing_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Movie list should be displayed on basis of \"Release Date\" in selected order",
  "keyword": "Then "
});
formatter.match({
  "location": "com.automation.bdd.selenium.steps.IMDBTopRatedMoviesTest.movie_list_should_be_displayed_on_basis_of_in_selected_order(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});