package com.automation.bdd.selenium.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;

@CucumberOptions(plugin = {"html:target/cucumber-html-report", "json:target/jsonReports/cucumber.json"}, 
									features = {"src/test/resources/features"},
									glue = {"com/automation/bdd/selenium/steps"}, 					
									monochrome = true, snippets = SnippetType.UNDERSCORE ,
									strict = true)

@RunWith(Cucumber.class)
public class CucumberRunnerTest{
	public static void generateReport() {
		
	}
}