package com.automation.bdd.selenium.steps;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.automation.bdd.selenium.dto.TopRatedMoviesDTO;
import com.automation.bdd.selenium.helper.TestHelper;
import com.automation.bdd.selenium.test.helper.ReUsableLibrary;
import com.automation.bdd.selenium.test.pages.IMDBTopRatedMoviePage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class IMDBTopRatedMoviesTest
{

	static Logger Log = LogManager.getLogger(IMDBTopRatedMoviesTest.class);

	private WebDriver driver;
	private TestHelper testHelper;
	private ReUsableLibrary reUsableLib;
	private IMDBTopRatedMoviePage imdbTopRatedMoviesPage;

	public IMDBTopRatedMoviesTest(SetupTearDownSteps setupTearDownSteps) {
		this.driver = setupTearDownSteps.getDriver();
		this.testHelper = setupTearDownSteps.getTestHelper();
		this.reUsableLib = setupTearDownSteps.getReusableLib();
		imdbTopRatedMoviesPage = new IMDBTopRatedMoviePage(driver, testHelper);
	}

	@Then("Page should show <number of movies> on page.")
	public void page_should_show_number_of_movies_on_page(List<String> expectedNumberOfMovies) throws Throwable {
		try {
			List<TopRatedMoviesDTO> topRatedMoviesList = imdbTopRatedMoviesPage.getTopRatedMoviesList();
			int topRatedMoviesCount = topRatedMoviesList.size();
			Log.info("Total Count Of Top Rated Movies : " + topRatedMoviesCount);
			Assert.assertEquals("IMDB Top Rated Movies Not Holding The Expected Number Of Movies List.",Double.parseDouble(expectedNumberOfMovies.get(1)), topRatedMoviesCount,0);
			reUsableLib.writeCSV(topRatedMoviesList, System.getProperty("user.dir")+ File.separator + "target/movieData.csv");
		} catch (Exception e) {
			Log.error("Error Happened To Verify Number Of Movies Count On Top Rated Movies List Page." + e);
			throw new Throwable("Error Happened To Verify Number Of Movies Count On Top Rated Movies List Page.");
		}
	}

	@Then("Movie list should be displayed on basis of {string} in {string}")
	public void movie_list_should_be_displayed_on_basis_of_in(String sortByOption, String defaultOrder) throws Throwable {
		try {

			imdbTopRatedMoviesPage.selectSortBy(sortByOption);
			Thread.sleep(4000);

			List<TopRatedMoviesDTO> movieListAfterSelectOptionFromWebActual = imdbTopRatedMoviesPage.getTopRatedMoviesList();
			List<TopRatedMoviesDTO> expectedTopRatedMovieWebTable = new ArrayList<TopRatedMoviesDTO>(movieListAfterSelectOptionFromWebActual);

			switch (sortByOption.trim()) {

			case "Ranking":
				Collections.sort(expectedTopRatedMovieWebTable);
				Assert.assertEquals("Ranking Default Ascending Order Not Working Properly.",expectedTopRatedMovieWebTable , movieListAfterSelectOptionFromWebActual);
				break;
			case "IMDb Rating":
				Comparator<TopRatedMoviesDTO> reverseOrderForRatings = Collections.reverseOrder(new TopRatedMoviesDTO.Ratings());
				Collections.sort(expectedTopRatedMovieWebTable, reverseOrderForRatings);
				Assert.assertEquals("IMDb Rating Default Descending Order Not Working Properly.",expectedTopRatedMovieWebTable , movieListAfterSelectOptionFromWebActual);
				break;
			case "Release Date":
				Comparator<TopRatedMoviesDTO> reverseOrderForReleaseDate = Collections.reverseOrder(new TopRatedMoviesDTO.MovieReleaseYear());
				Collections.sort(expectedTopRatedMovieWebTable, reverseOrderForReleaseDate);
				Assert.assertEquals("Release Date Default Descending Order Not Working Properly.",expectedTopRatedMovieWebTable , movieListAfterSelectOptionFromWebActual);
				break;
			case "Number of Ratings":
				Comparator<TopRatedMoviesDTO> reverseOrderForNumberOfUserRatings = Collections.reverseOrder(new TopRatedMoviesDTO.UserRatingComparator());
				Collections.sort(expectedTopRatedMovieWebTable, reverseOrderForNumberOfUserRatings);
				Assert.assertEquals("User Rating Default Descending Order Not Working Properly.",expectedTopRatedMovieWebTable , movieListAfterSelectOptionFromWebActual);
				break;
			case "Your Rating":
				Collections.sort(expectedTopRatedMovieWebTable);
				Assert.assertEquals("Your Rating Default Descending Order Not Working Properly.",expectedTopRatedMovieWebTable , movieListAfterSelectOptionFromWebActual);
				break;
			default:
				throw new UnsupportedOperationException("Sorted Option Not Listed.");
			}
		} catch (Throwable e) {
			if (e instanceof AssertionError) {
				Log.error("Assertion Error Happened On Sort By And Default Order." + e);
				throw new Throwable("Assertion Error Happened On Sort By And Default Order." + e);
			} else {
				Log.error("Error Happened On Sort By And Default Order." + e);
				throw new Throwable("Error Happened On Sort By And Default Order.");
			}
		}
	}

	@When("Click on Order changing button.")
	public void click_on_Order_changing_button() throws Throwable {
		try {
			Log.info("Click on order changing button.");
			imdbTopRatedMoviesPage.changeOrder();
		} catch (Throwable e) {
			Log.error("Error Happened To Change The Order Status Changing Button." + e);
			throw new Throwable("Error Happened To Change The Order Status Changing Button.");
		}
	}

	@Then("Movie list should be displayed on basis of {string} in selected order")
	public void movie_list_should_be_displayed_on_basis_of_in_selected_order(String sortByOption) throws Throwable {
		try {
			List<TopRatedMoviesDTO> movieListAfterChangeTheOrderWebActual = imdbTopRatedMoviesPage.getTopRatedMoviesList();

			List<TopRatedMoviesDTO> expectedTopRatedMoviesWebTable = new ArrayList<TopRatedMoviesDTO>(movieListAfterChangeTheOrderWebActual);

			switch (sortByOption.trim()) {

			case "Ranking":
				Comparator<TopRatedMoviesDTO> reverseOrderForRanking = Collections.reverseOrder(new TopRatedMoviesDTO.SerialNumberComparator());
				Collections.sort(expectedTopRatedMoviesWebTable, reverseOrderForRanking);
				Assert.assertEquals("Ranking Descending Order Not Working Properly.",expectedTopRatedMoviesWebTable , movieListAfterChangeTheOrderWebActual);
				break;
			case "IMDb Rating":
				Collections.sort(expectedTopRatedMoviesWebTable, new TopRatedMoviesDTO.Ratings());
				Assert.assertEquals("IMDb Rating Ascending Order Not Working Properly.",expectedTopRatedMoviesWebTable , movieListAfterChangeTheOrderWebActual);
				break;
			case "Release Date":
				Collections.sort(expectedTopRatedMoviesWebTable, new TopRatedMoviesDTO.MovieReleaseYear());
				Assert.assertEquals("Release Date Ascending Order Not Working Properly.",expectedTopRatedMoviesWebTable , movieListAfterChangeTheOrderWebActual);
				break;
			case "Number of Ratings":
				Collections.sort(expectedTopRatedMoviesWebTable, new TopRatedMoviesDTO.UserRatingComparator());
				Assert.assertEquals("User Rating Ascending Order Not Working Properly.",expectedTopRatedMoviesWebTable , movieListAfterChangeTheOrderWebActual);
				break;
			case "Your Rating":
				Comparator<TopRatedMoviesDTO> reverseOrderForSerialNumber = Collections.reverseOrder(new TopRatedMoviesDTO.SerialNumberComparator());
				Collections.sort(expectedTopRatedMoviesWebTable, reverseOrderForSerialNumber);
				Assert.assertEquals("Your Rating Ascending Order Not Working Properly.",expectedTopRatedMoviesWebTable , movieListAfterChangeTheOrderWebActual);
				break;
			default:
				throw new UnsupportedOperationException("Sorted Option Not Listed.");
			}
		} catch (Throwable e) {		
			if (e instanceof AssertionError) {
				Log.error("Assertion Error Happened On Sort By And Changed Order." + e);
				throw new Throwable("Assertion Error Happened On Sort By And Changed Order." + e);
			} else {
				Log.error("Error Happened On Sort By And Selected Order."+ e);
				throw new Throwable("Error Happened On Sort By And Selected Order.");
			}
		}
	}
}
