package com.automation.bdd.selenium.steps;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.openqa.selenium.WebDriver;

import com.automation.bdd.selenium.driver.BrowserType;
import com.automation.bdd.selenium.driver.TestConfigurationSetUp;
import com.automation.bdd.selenium.driver.WebDriverFactory;
import com.automation.bdd.selenium.helper.TestAutomationConstants;
import com.automation.bdd.selenium.helper.TestHelper;
import com.automation.bdd.selenium.propertieshelper.TestConfigurationPropertiesReader;
import com.automation.bdd.selenium.test.helper.ReUsableLibrary;
import com.google.common.base.Strings;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.Status;
import lombok.Data;

@Data
public class SetupTearDownSteps {

	/**
	 * Initialization of Log4j configuration. 
	 */
	private static Logger Log = LogManager.getLogger(SetupTearDownSteps.class);

	static{
		LoggerContext context = (LoggerContext) LogManager.getContext(false);
		File file = new File(System.getProperty("user.dir")+"/src/test/resources/config/log4j2.xml");
		context.setConfigLocation(file.toURI());
		Log.info("Log Properties Initialized");
	}

	private WebDriver driver;
	private TestHelper testHelper;
	private ReUsableLibrary reusableLib;
	private TestConfigurationPropertiesReader testConfigProperties;
	private TestConfigurationSetUp testConfig;
	private String message;
	public static ThreadLocal<List<byte[]>> threadedScreenshots = new ThreadLocal<List<byte[]>>();
	List<byte[]> screenshots;
	/**
	 * Below setupTest() method executed before start of test scenario execution.
	 * Desired capabilities as per browser selection , Property file, 
	 * Test Automation Constants and TestHelper class for reusable methods.
	 * Also open the desired browser with AUT(Application Under Test).
	 * @throws Throwable 
	 */
	@Before
	public void setupTest(Scenario scenario) throws Throwable
	{
		testConfigProperties = TestConfigurationPropertiesReader.getInstance();
		testConfig = TestConfigurationSetUp.getInstance();
		synchronized (this) {
			testConfig.loadTestConfig(testConfigProperties);
			driver = WebDriverFactory.getThreadedWebDriver(BrowserType.valueOf(testConfig.getBrowser()), testConfig);
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(TestAutomationConstants.AJAX_MAX_EXPLICIT_WAIT_TIME_OUT, TimeUnit.SECONDS);
			testHelper = new TestHelper(driver);
			reusableLib = new ReUsableLibrary(driver, testHelper);

			screenshots = threadedScreenshots.get();
			if(screenshots == null){
				screenshots = new ArrayList<byte[]>();
				threadedScreenshots.set(screenshots);
			}

			Log.info("===============================================================================================");
			Log.info("Feature Name : " +  scenario.getId());
			Log.info("-----------------------------------------------------------------------------------------------");
			Log.info("Scenario Name : " + scenario.getName());
			Log.info("-----------------------------------------------------------------------------------------------");
			Log.info("Tag Name : " + scenario.getSourceTagNames().toString());
			Log.info("===============================================================================================");

			String imdbUrl = testConfig.getApplicationURL();
			Log.info("Going to open IMDB application and URL : " + imdbUrl);

			if (Strings.isNullOrEmpty(imdbUrl)) {
				throw new RuntimeException("IMDB Application URL Not Provided. ");
			} else {
				try {
					driver.get(imdbUrl);
				} catch (Exception e) {
					Log.error("Error Happened To Open IMDB Home Page." + e);
					try {
						driver.get(imdbUrl);
					} catch (Exception e1) {
						Log.error("Error Happened To Open IMDB Home Page." + e1);
						throw new Throwable("Error Happened To Open IMDB Home Page.");
					}
				}
			}
		}
	}

	/**
	 * tearDown(Scenario result) method executed at the end of every test scenario.
	 * This method used to take screen shot for every failed scenario.
	 * Also clear the browser cookies and close all the browser session.
	 * @param result
	 * @throws Throwable 
	 */
	@After
	public void tearDown(Scenario result) throws Throwable
	{
		synchronized (result) {
			if (driver==null) {
				throw new Throwable("driver not instantiatate." );
			}
			try {
				if(result.isFailed()){
					message = result.getName();
					result.write(result.toString());
					result.write(Status.FAILED.toString());
					result.write("ScreenShot taken for failed step for " + message);

					screenshots = threadedScreenshots.get();
					if (screenshots != null && screenshots.size()>0) {
						for (byte[] screenshot : screenshots) {
							result.embed(screenshot, "image/png",message);
						}
					} else {
						byte[] screenshot = testHelper.takeScreenShot(driver);
						result.embed(screenshot,"image/png", message);
					}
				}
			}

			catch (Exception e) {
				Log.error("Error Happend To Close Browser. " + e);
			}
			finally {
				closeBrowser();
				threadedScreenshots.remove();
			}
		}
	}

	private void closeBrowser() throws Throwable{
		driver.manage().deleteAllCookies();
		//		driver.close();
		driver.quit();
		WebDriverFactory.removeThreadedDriver();
	}
}