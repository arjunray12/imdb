package com.automation.bdd.selenium.steps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import com.automation.bdd.selenium.helper.TestHelper;
import com.automation.bdd.selenium.test.pages.IMDBHomePage;
import com.automation.bdd.selenium.test.pages.IMDBTopRatedMoviePage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class CommonSteps {
	static Logger Log = LogManager.getLogger(IMDBTopRatedMoviesTest.class);

	private WebDriver driver;
	private TestHelper testHelper;
	private IMDBHomePage imdbHomePage;
	private IMDBTopRatedMoviePage imdbTopRatedmoviesPage;

	public CommonSteps(SetupTearDownSteps setupTearDownSteps) {
		this.driver = setupTearDownSteps.getDriver();
		this.testHelper = setupTearDownSteps.getTestHelper();
		imdbHomePage = new IMDBHomePage(driver, testHelper);
		imdbTopRatedmoviesPage = new IMDBTopRatedMoviePage(driver, testHelper);
	}

	@Given("User Navigate To Top Rated Movies page from {string} under {string} section.")
	public void user_Navigate_To_Top_Rated_Movies_page_from_under_section(String linkTxt, String section) throws Throwable {
		try {
			imdbHomePage.navigateTo(section, linkTxt, IMDBTopRatedMoviePage.class);
		} catch (Exception e) {
			Log.error("Error Happened To Navigate " + linkTxt + " Under " + section + " . " + e);
			throw new Throwable("Error Happened To Navigate " + linkTxt + " Under " + section + " . " );
		}
	}

	@Then("Top Rated Movies page title should be {string}")
	public void top_rated_movies_page_title_should_be(String expectedPageTitle) throws Throwable {
		try {
			Assert.assertEquals("Top Rated Movies Page Title Not Matched.", expectedPageTitle.trim(), imdbTopRatedmoviesPage.getPageTitle());
		} catch (Throwable e) {
			if (e instanceof AssertionError) {
				Log.error("Assertion Error : " + e.getMessage());
			} else {
				Log.error("Error Happened To Validate Page Title Of 'Top Rated Movies On IMDB.'" + e);
				throw new Throwable("Error Happened To Validate Page Title Of 'Top Rated Movies On IMDB.'");
			}
		}
	}
}
