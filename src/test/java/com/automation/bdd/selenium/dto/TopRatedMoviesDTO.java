package com.automation.bdd.selenium.dto;

import java.util.Comparator;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import lombok.Data;

@Data
public class TopRatedMoviesDTO implements Comparable<TopRatedMoviesDTO>{
	
	private Integer serialNumber;
	private Integer userRating;
	
	@CsvBindByName(column = "Movie Name",required = true)
	@CsvBindByPosition(position = 0)
	private String name;
	
	@CsvBindByName(column = "Release Year",required = true)
	@CsvBindByPosition(position = 1)
	private Integer movieReleaseYear;
	
	@CsvBindByName(column = "IMDB Rating",required = true)
	@CsvBindByPosition(position = 2)
	private Double ratings;

	@Override
	public int compareTo(TopRatedMoviesDTO o) {
		return this.serialNumber.compareTo(o.serialNumber);
	}
	
	public static class SerialNumberComparator implements Comparator<TopRatedMoviesDTO>{

		@Override
		public int compare(TopRatedMoviesDTO o1, TopRatedMoviesDTO o2) {
			return o1.getSerialNumber().compareTo(o2.getSerialNumber());
		}
	}
	
	public static class UserRatingComparator implements Comparator<TopRatedMoviesDTO>{

		@Override
		public int compare(TopRatedMoviesDTO o1, TopRatedMoviesDTO o2) {
			return o1.getUserRating().compareTo(o2.getUserRating());
		}
	}
	
	public static class Name implements Comparator<TopRatedMoviesDTO>{

		@Override
		public int compare(TopRatedMoviesDTO o1, TopRatedMoviesDTO o2) {
			return o1.getName().compareTo(o2.getName());
		}
	}
	
	public static class MovieReleaseYear implements Comparator<TopRatedMoviesDTO>{

		@Override
		public int compare(TopRatedMoviesDTO o1, TopRatedMoviesDTO o2) {
			return o1.getMovieReleaseYear().compareTo(o2.getMovieReleaseYear());
		}
	}
	
	public static class Ratings implements Comparator<TopRatedMoviesDTO>{

		@Override
		public int compare(TopRatedMoviesDTO o1, TopRatedMoviesDTO o2) {
			return o1.getRatings().compareTo(o2.getRatings());
		}
	}
}