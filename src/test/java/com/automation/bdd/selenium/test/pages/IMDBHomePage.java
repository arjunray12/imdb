package com.automation.bdd.selenium.test.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.automation.bdd.selenium.helper.TestAutomationConstants;
import com.automation.bdd.selenium.helper.TestHelper;

public class IMDBHomePage {
	
	private static final Logger logger = LogManager.getLogger(IMDBHomePage.class);
	
	private final WebDriver driver;
	private final TestHelper testHelper;
	
	@FindBy(css  = "#imdbHeader-navDrawerOpen--desktop")
	WebElement hamburgerMenu;
	
	@FindBy(xpath = ".//div[@data-testid='list-container']//a[@role='menuitem'][not(contains(@class,'hideL'))]")
	List<WebElement> all_Visible_Link_From_AllSection_List;
	
	@FindBy(xpath = ".//div[@data-testid='panel-content']//label")
	List<WebElement> sectionList;
	
	public IMDBHomePage(WebDriver driver, TestHelper testHelper) {
		this.driver = driver; 
		this.testHelper = testHelper;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, TestAutomationConstants.TIMEOUT_MAX), this);
	}
	
	public <T> T navigateTo(String section, String linkTxt,Class<T> expectedPage) throws Exception {
		testHelper.mouseMoveAndClick(driver, hamburgerMenu);
		for (WebElement sectionElement : sectionList) {
			String sectionName = sectionElement.findElement(By.xpath("./span[2]")).getText().trim();
			if (StringUtils.equalsIgnoreCase(section.trim(), sectionName)) {
				for (WebElement sectionLink : all_Visible_Link_From_AllSection_List) {
					String linkName = sectionLink.findElement(By.xpath("./span")).getText().trim();
					if (StringUtils.equalsIgnoreCase(linkTxt.trim(), linkName)) {
						logger.info("Click On Link From Expanded Hamburger Menu " + linkName );
						testHelper.click(sectionLink);
						break;
					}
				}
				break;
			}
		}
		
		return expectedPage.getDeclaredConstructor(WebDriver.class,TestHelper.class).newInstance(driver,testHelper);
	} 
}