package com.automation.bdd.selenium.test.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.automation.bdd.selenium.dto.TopRatedMoviesDTO;
import com.automation.bdd.selenium.helper.TestAutomationConstants;
import com.automation.bdd.selenium.helper.TestHelper;
import com.automation.bdd.selenium.webtable.helper.WebTable;
import com.automation.bdd.selenium.webtable.helper.WebTableRow;

public class IMDBTopRatedMoviePage {

	private static final Logger logger = LogManager.getLogger(IMDBTopRatedMoviePage.class);
	
	private final WebDriver driver;
	private final TestHelper testHelper;

	@FindBy(xpath = ".//div[@class='article']//h1")
	WebElement topRatedMoviesPageTitle;

	@FindBy(xpath = ".//table[@class='chart full-width']")
	WebElement topRatedMoviesTable;
	
	@FindBy(xpath = ".//span[contains(@class,'global-sprite lister-sort-reverse')]")
	WebElement changeOrder;
	
	@FindBy(xpath = ".//select[@id='lister-sort-by-options']")
	WebElement sortBySelect;
	
	public IMDBTopRatedMoviePage(WebDriver driver, TestHelper testHelper) {
		this.driver = driver; 
		this.testHelper = testHelper;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, TestAutomationConstants.TIMEOUT_MAX), this);
	}

	public String getPageTitle() {
		return testHelper.getElementText(topRatedMoviesPageTitle).trim();
	}

	public List<TopRatedMoviesDTO> getTopRatedMoviesList() throws Throwable {

		List<TopRatedMoviesDTO> topRatedMovies = new ArrayList<TopRatedMoviesDTO>();
		List<WebTableRow> topRatedMoviesRows = WebTable.getTable(topRatedMoviesTable).getTableBody().getRows();
		try {
			for (WebTableRow row : topRatedMoviesRows) {
				TopRatedMoviesDTO movie = new TopRatedMoviesDTO();
				String serialNumber = row.getCell(1).getText().trim();
				movie.setSerialNumber(Integer.parseInt(testHelper.extractStringRegEx(serialNumber, "\\d*", 0)));
				movie.setName(row.getCell(1).getCell("./a").getText().trim());
				movie.setMovieReleaseYear(Integer.parseInt(row.getCell(1).getCell("./span").getText().replace("(", "").replace(")", "").trim()));
				movie.setRatings(Double.parseDouble(row.getCell(2).getCell("./strong").getText().trim()));
				String userRatingCount = row.getCell(2).getCell("./strong").getAttribute("title").trim();
				movie.setUserRating(Integer.parseInt(testHelper.extractStringRegEx(userRatingCount, "\\d*,.*\\d", 0).replace(",", "")));
				topRatedMovies.add(movie);
			}

			logger.debug("Top Rated Movies List Is : " + topRatedMovies);
		} catch (Exception e) {
			logger.error("Error Happened To Extract The Data From IMDB Top 250 Movie List WebTable." + e);
			throw new Throwable("Error Happened To Extract The Data From IMDB Top 250 Movie List WebTable.");
		}
		return topRatedMovies;
	}
	
	public IMDBTopRatedMoviePage selectSortBy(String sortByOption) throws Exception {
		logger.info("Going to select option : " + sortByOption);
		testHelper.selectElementFromDropDown(sortBySelect, sortByOption);
		return new IMDBTopRatedMoviePage(driver, testHelper);
	}
	
	public IMDBTopRatedMoviePage changeOrder() throws Exception {
		testHelper.click(changeOrder);
		return new IMDBTopRatedMoviePage(driver, testHelper);
	}
}