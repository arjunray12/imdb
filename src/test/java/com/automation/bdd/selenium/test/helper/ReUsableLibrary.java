package com.automation.bdd.selenium.test.helper;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.automation.bdd.selenium.helper.TestHelper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class ReUsableLibrary {

	static Logger Log = LogManager.getLogger(ReUsableLibrary.class);

	WebDriver driver;
	TestHelper testHelper;

	public ReUsableLibrary(WebDriver driver, TestHelper testHelper) {
		this.driver = driver;
		this.testHelper = testHelper;
	}
	
	public <T> void writeCSV(List<T> data,String filePath) throws IOException {
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))){
			StatefulBeanToCsvBuilder<T> builder = new StatefulBeanToCsvBuilder<T>(writer);
			StatefulBeanToCsv<T> beanWriter = builder.withSeparator(CSVWriter.DEFAULT_SEPARATOR).withApplyQuotesToAll(false).build();
			try {
				beanWriter.write(data);
				writer.flush();

			} catch (CsvDataTypeMismatchException | CsvRequiredFieldEmptyException  e) {
				throw new RuntimeException("Failed to download admin file");
			}
		}
	}
}