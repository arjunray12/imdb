Feature: Verify The Top Rated Movies Page Of IMDB.

  Background: 
    Given User Navigate To Top Rated Movies page from "Top Rated Movies" under "Movies" section.
    Then Top Rated Movies page title should be "Top Rated Movies"

  Scenario: Verify Top Rated Movies List Count.
    Then Page should show <number of movies> on page.
      | number of movies |
      |              250 |

  Scenario Outline: Verify Top Rated Movies List Sort By Options and Order.
    Then Movie list should be displayed on basis of "<sort by option>" in "<default order>"
    When Click on Order changing button.
    Then Movie list should be displayed on basis of "<sort by option>" in selected order

    Examples: 
      | sort by option    | default order    |
      | Ranking           | Ascending Order  |
      | IMDb Rating       | Descending Order |
      | Release Date      | Descending Order |
      | Number of Ratings | Descending Order |
      | Your Rating       | Descending Order |
