package com.automation.bdd.selenium.webtable.helper;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WebTablePart {
	private WebElement tablePartElement;

	public WebTablePart(WebElement tablePartElement) {
		this.tablePartElement = tablePartElement;
	}

	public WebTableRow getRow(int index) {
		return new WebTableRow(getRowElement(index));
	}

	public WebElement getRowElement(int index) {
		WebElement rowElement = (WebElement) tablePartElement.findElements(By.xpath("./tr")).get(index);
		return rowElement;
	}

	public WebTableRow getRow(int index, String trXpath) {
		return new WebTableRow(getRowElement(index, trXpath));
	}

	public WebElement getRowElement(int index, String trXpath) {
		WebElement rowElement = (WebElement) tablePartElement.findElements(By.xpath(trXpath)).get(index);
		return rowElement;
	}

	public int getRowCount() {
		return tablePartElement.findElements(By.xpath("./tr")).size();
	}

	public List<WebTableRow> getRows(String rowXpath) {
		List<WebTableRow> rows = new ArrayList<WebTableRow>();
		for (WebElement webTableRowElement : tablePartElement.findElements(By.xpath(rowXpath))) {
			rows.add(new WebTableRow(webTableRowElement));
		}
		return rows;
	}

	public List<WebTableRow> getRows() {
		List<WebTableRow> rows = new ArrayList<WebTableRow>();
		for (WebElement webTableRowElement : tablePartElement.findElements(By.xpath("./tr"))) {
			rows.add(new WebTableRow(webTableRowElement));
		}
		return rows;
	}
	
	public int getRowCount(String rowTagName) {
		return tablePartElement.findElements(By.tagName(rowTagName)).size();
	}
}
