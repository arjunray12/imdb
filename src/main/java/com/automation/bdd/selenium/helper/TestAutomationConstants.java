package com.automation.bdd.selenium.helper;

/**
 * This is used for constant but value is overrided if 
 * it is configured from  test-config.properties.
 */
public class TestAutomationConstants {
	public static int TIMEOUT_MAX = 20;
	public static int TIMEOUT_AVG = 10;
	public static int TIMEOUT_MIN = 5;
	public static int AJAX_MAX_EXPLICIT_WAIT_TIME_OUT = 60;
	public static int AJAX_AVG_EXPLICIT_WAIT_TIME_OUT = 30;
	public static int AJAX_MIN_EXPLICIT_WAIT_TIME_OUT = 15;
	public static int POLLING = 2;
}