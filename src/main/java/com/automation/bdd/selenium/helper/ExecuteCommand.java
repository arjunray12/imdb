package com.automation.bdd.selenium.helper;

/**
 * @author ArjunRay
 *
 * @param <T>
 * 
 * This Interface created to use for wait with custom logic.
 */
public interface ExecuteCommand<T> {
	
	public T execute();

}
