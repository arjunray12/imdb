package com.automation.bdd.selenium.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class TestHelper {

	Logger Log = LogManager.getLogger(TestHelper.class);
	WebDriver driver;

	public TestHelper(WebDriver driver)
	{
		this.driver = driver;
	}

	public void sync(WebDriver driver, WebElement element) throws Exception
	{
		try
		{
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(ExpectedConditions.visibilityOf(element));
		}
		catch (RuntimeException e)
		{
			this.Log.error(element + " Not visible on page : " + driver.getTitle());
			throw e;
		}
	}

	public void syncWithLocation(WebDriver driver,  WebElement element) throws Exception
	{
		final WebElement chkElement = element;
		try
		{
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(new ExpectedCondition<Boolean>() {

				@Override
				public Boolean apply(WebDriver driver) {
					return (chkElement.getLocation().getX()>0||chkElement.getLocation().getY()>0);
				}
			});
		}
		catch (RuntimeException e)
		{
			this.Log.error(element + " Not visible on page : " + driver.getTitle());
			throw e;
		}
	}

	public boolean syncWithDisplayLocation(WebDriver driver,  WebElement element) throws Exception
	{
		final WebElement chkElement = element;
		try
		{
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(new ExpectedCondition<Boolean>() {

				@Override
				public Boolean apply(WebDriver driver) {
					return ((chkElement.getLocation().getX()>0||chkElement.getLocation().getY()>0) && chkElement.isDisplayed());
				}
			});
			return true;
		}
		catch (RuntimeException e)
		{
			this.Log.error(element + " Not visible on page : " + driver.getTitle());
			throw e;
		}
	}

	public void syncClickable(WebDriver driver, WebElement element) throws Exception
	{
		try
		{
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		}
		catch (Exception e)
		{
			this.Log.error(element + " Not clickable on page : " + driver.getTitle());
			throw e;
		}
	}

	public void clearAndType(WebElement element, String str) throws Exception
	{
		try
		{
			element.clear();
			Thread.sleep(1000);
			element.sendKeys(new CharSequence[] { str.toString() });
		}
		catch (Exception e)
		{
			this.Log.error("Couldnot eble to send text on element: " + element);		
			throw e;
		}
	}

	public void type(WebElement element, String str) throws Exception
	{
		try
		{
			element.sendKeys(new CharSequence[] { str.toString() });
		}
		catch (Exception e)
		{
			this.Log.error("Couldnot eble to send text on element: " + element);		
			throw e;
		}
	}

	public void typeWithJavaScript(WebElement element, String str) throws Exception
	{
		try
		{
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].value='"+ str +"'", element);
		}
		catch (Exception e)
		{
			this.Log.error("Couldnot eble to send text on element: " + element);
			throw e;
		}
	}

	public void typeSequentially(WebElement element, String value) throws InterruptedException{
		String val = value; 
		WebElement txt_Element = element;
		txt_Element.clear();

		for (int i = 0; i < val.length(); i++){
			char c = val.charAt(i);
			Thread.sleep(1000);
			element.sendKeys(String.valueOf(c));
		} 
	}

	public void removeReadOnly(WebElement element) {
		try
		{
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].removeAttribute('readonly',0);", element);
		}
		catch (Exception e)
		{
			this.Log.error("Couldnot able to send text on element: " + element);
			throw e;
		}
	}

	public void click(WebElement element) throws Exception
	{
		try
		{
			element.click();
			Thread.sleep(1000L);
		}
		catch (Exception e)
		{
			this.Log.error("Element : " + element + " Not Clickable");
			throw e;
		}
	}

	public void clickWithJavaScript(WebDriver driver, WebElement element)throws Throwable
	{
		try
		{
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", new Object[] { element });
		}
		catch (Exception e)
		{
			this.Log.error("Error Happened To Click On " + element + " Using JavaScript. " + e);
			throw new Throwable("Error Happened To Click On " + element + " Using JavaScript. ");
		}
	}

	public boolean isElementDisplay(WebElement element)
	{
		try
		{
			element.isDisplayed();
			return true;
		}
		catch (Exception e) {}
		return false;
	}

	public boolean isElementDisplayWithAttribute(WebElement element, String attribute)
	{
		boolean isDisplayed = false;
		try
		{
			if (element.getAttribute(attribute.trim()).contains("none")) {
				isDisplayed = true;
			} else {
				isDisplayed = false;
			}
		}
		catch (Exception e) {}
		return isDisplayed;
	}


	public boolean isElementDisplayWithStyleAttribute(WebElement element)
	{
		boolean isDisplayed = false;
		try
		{
			if (!element.getAttribute("style").contains("none")) {
				isDisplayed = true;
			} 
		}
		catch (Exception e) {}
		return isDisplayed;
	}

	public void selectElementFromDropDown(WebElement element, String visibleText) throws Exception
	{
		Select dropDown = new Select(element);
		dropDown.selectByVisibleText(visibleText);
	}

	public void selectElementByValueFromDropDown(WebElement element, String value)
	{
		Select dropDown = new Select(element);
		dropDown.selectByValue(value);
	}

	public void selectElementByValueFromDropDownWithClick(WebElement element, String value)
			throws Throwable
	{
		Select dropDown = new Select(element);
		List<WebElement> dropDownOptions = dropDown.getOptions();
		for (WebElement webElement : dropDownOptions)
		{
			System.out.println(webElement.getAttribute("value"));
			if (StringUtils.equalsIgnoreCase(value.trim(), webElement.getAttribute("value").trim()))
			{
				System.out.println(webElement.getAttribute("value"));
				click(webElement);
				break;
			}
		}
	}

	public void selectElementByValueFromDropDownWithIndex(WebElement element, String value)
			throws Throwable
	{
		Select dropDown = new Select(element);
		List<WebElement> dropDownOptions = dropDown.getOptions();
		for (int index = 0; index < dropDownOptions.size(); index++)
		{
			System.out.println(((WebElement)dropDownOptions.get(index)).getAttribute("value"));
			click((WebElement)dropDownOptions.get(index));
			Thread.sleep(1000L);
			if (StringUtils.equalsIgnoreCase(value.trim(), ((WebElement)dropDownOptions.get(index)).getAttribute("value").trim()))
			{
				click((WebElement)dropDownOptions.get(index));
				Thread.sleep(1000L);
				break;
			}
		}
	}

	public void selectOptionWithIndex(List<WebElement> dropDownElements, int index) throws Exception
	{
		List<WebElement> options = dropDownElements;
		try
		{
			Thread.sleep(1000);
			WebElement element = options.get(index);
			//			JavascriptUtils.bringIntoView(element);
			click(element);
		}
		catch (Exception e)
		{
			this.Log.error("Issue On Select WebElement : " + dropDownElements + "For Index " + index);
			throw e;
		}
	}

	public void selectRandomOption(List<WebElement> dropDownElements) throws Exception
	{

		int index = 0;
		List<WebElement> options = dropDownElements;
		int option_Size = options.size();
		try
		{
			Thread.sleep(1000);

			index = new Random().nextInt(option_Size - 0) + 0;
			//index = index < 0 ? 0 : dropDownElements.size() - 1;
			WebElement element = options.get(index);
			//			JavascriptUtils.bringIntoView(element);
			click(element);
		}
		catch (Exception e)
		{
			this.Log.error("Issue On Select WebElement : " + dropDownElements + "For Index " + index);
			throw e;
		}
	}

	public WebElement getRandomOption(List<WebElement> dropDownElements) throws Exception
	{

		int index = 0;
		WebElement element = null;
		List<WebElement> options = dropDownElements;
		int option_Size = options.size();
		try
		{
			Thread.sleep(1000);	
			index = new Random().nextInt(option_Size);
			index = index <= 0 ? 0 : index;
			element = options.get(index);
			//			JavascriptUtils.bringIntoView(element);
		}
		catch (Exception e)
		{
			this.Log.error("Issue On Select WebElement : " + dropDownElements + "For Index " + index);
			throw e;
		}
		return element;
	}

	public String getElementInnerText(WebElement element)
	{
		String innerTextValue = null;
		try {
			innerTextValue = Strings.isNullOrEmpty(element.getAttribute("innerText")) == false
					? element.getAttribute("innerText").replace("\n", "").replace("\r", "").trim()
							: element.getAttribute("innerText");
		} catch (Exception e) {
			Log.error("Error Happened To Get Inner Text From Element : " + element);
			throw e;
		}
		return innerTextValue;
	}

	public String getElementText(WebElement element){
		String textValue = null;
		try {
			textValue = element.getText().trim();
		} catch (Exception e) {
			Log.error("Error Happened To Get Text From Element : " + element);
			throw e;
		}
		return textValue;
	}

	public String getElementTextWithAttribute(WebElement element, String attribute){
		String textValue = null;
		try {
			textValue = element.getAttribute(attribute).trim();
		} catch (Exception e) {
			Log.error("Error Happened To Get Text From Element : " + element);
			throw e;
		}
		return textValue;
	}

	public String getTextFromTextBox(WebElement element)
	{
		return element.getAttribute("value");
	}

	public String getTextAreaText(WebDriver driver, WebElement textAreaElement)
	{
		String text = null;
		try
		{
			text = textAreaElement.getAttribute("value");
		}
		catch (RuntimeException e)
		{
			this.Log.error("Not Able To Extracting Text From Element : " + textAreaElement + " on page : " + driver.getTitle());
			throw e;
		}
		return text;
	}

	public String getCurrentValueFromDropDown(WebElement element)
	{
		Select dropDown = new Select(element);
		WebElement value = dropDown.getFirstSelectedOption();
		String text = value.getText();
		return text;
	}

	public String getUniqueID()
	{
		return UUID.randomUUID().toString();
	}

	public void mouseMoveAndClick(WebDriver driver, WebElement element) throws InterruptedException
	{
		try
		{
			Actions action = new Actions(driver);
			action.moveToElement(element).click().build().perform();
			Thread.sleep(1000L);
		}
		catch (RuntimeException e)
		{
			this.Log.error("Mouse Click Using Action On : " + element + "Not Happen");
			throw e;
		}
	}

	public void mouseMove(WebDriver driver, WebElement element)
			throws InterruptedException
	{
		try
		{
			Actions action = new Actions(driver);
			action.moveToElement(element).perform();
			Thread.sleep(1000L);
			action.moveToElement(element).build().perform();
			Thread.sleep(1000L);
		}
		catch (RuntimeException e)
		{
			this.Log.error("Mouse Over Using Action On : " + element + "Not Happen");
			throw e;
		}
	}

	public void sync(WebDriver driver, List<WebElement> elements)
	{
		try
		{

			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(ExpectedConditions.visibilityOfAllElements(elements));
		}
		catch (RuntimeException e)
		{
			this.Log.error(elements + " Not visible on page : " + driver.getTitle());
			throw e;
		}
	}

	public void syncInvisible(WebDriver driver, List<WebElement> elements)
	{
		try
		{
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(ExpectedConditions.invisibilityOfAllElements(elements));
		}
		catch (RuntimeException e)
		{
			this.Log.error(elements + " visible on page : " + driver.getTitle());
			throw e;
		}
	}

	public void syncInvisible(WebDriver driver, WebElement webElement)
	{
		final WebElement element = webElement;
		try
		{
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.TIMEOUT_MAX);
			wait.until(new ExpectedCondition<Boolean>() {

				public Boolean apply(WebDriver arg0) {
					try
					{
						return Boolean.valueOf(!element.isDisplayed());
					}
					catch (NoSuchElementException e)
					{
						return Boolean.valueOf(true);
					}
					catch (StaleElementReferenceException e)
					{
						return Boolean.valueOf(true);
					}
					catch (NullPointerException e) {}
					return Boolean.valueOf(true);	        
				}});
		}
		catch (RuntimeException e)
		{
			this.Log.error(element + " visible on page : " + driver.getTitle());
			throw e;
		}
	}

	public void waitUntillCommandReturnsTrue(ExecuteCommand<Boolean> executeCommand, TimeUnit timeUnit, long retryInterval, long timeOut)
			throws TimeoutException
	{
		boolean result = false;

		long startTime = System.currentTimeMillis();
		long maxWaitTime = startTime + TimeUnit.MILLISECONDS.convert(timeOut, timeUnit);
		while ((maxWaitTime > System.currentTimeMillis()) && (!result))
		{
			result = ((Boolean)executeCommand.execute()).booleanValue();
			if (!result) {
				try
				{
					Thread.sleep(TimeUnit.MILLISECONDS.convert(retryInterval, timeUnit));
				}
				catch (InterruptedException e1)
				{
					e1.printStackTrace();
				}
			}
		}
		if (!result) {
			throw new TimeoutException();
		}
	}

	public int randomInteger(int max, int min){
		return new Random().nextInt(max-min) + min;	
	}

	public String getDate(Date date){
		return new SimpleDateFormat("MM/dd/yyyy").format(date);
	}

	public Date getDate(String date) throws ParseException{
		return new SimpleDateFormat("MM/dd/yyyy").parse(date);
	}

	public String getFutureDate(Date date, int days){
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.add(Calendar.DATE, days);
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public String getPastDate(Date date, int days){
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		calender.add(Calendar.DATE, -days);
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public String getCurrentWeekDate(){
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		calender.setFirstDayOfWeek(Calendar.SUNDAY);
		System.out.println("Date " + calender.getTime());
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public String getCurrentMonthDate(){
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.DAY_OF_MONTH, 1);
		System.out.println("Date " + calender.getTime());
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public String getCurrentYearDate(){
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.DAY_OF_YEAR, 1);
		System.out.println("Date " + calender.getTime());
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public String getFirstDayOfQuarter() {
		Calendar calender = Calendar.getInstance();
		calender.setTime(calender.getTime());
		calender.set(Calendar.MONTH, calender.get(Calendar.MONTH)/3 * 3);
		calender.set(Calendar.DAY_OF_MONTH, 1);
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public String getTimeStamp(){
		Date date = new Date();
		String timeStamp = new Timestamp(date.getTime()).toString().replace("-", "").replace(":", "").replace(".", "").replace(" ", "");		
		return timeStamp.substring(0, 12);
	}

	public String getAddedDay(Date date, int day) throws ParseException{
		Calendar calender = Calendar.getInstance();
		calender.setTime(date);
		System.out.println(calender.getTime());
		calender.add(Calendar.DATE, day);
		System.out.println(calender.getTime());
		return new SimpleDateFormat("MM/dd/yyyy").format(calender.getTime());
	}

	public boolean waitAngularLoaded(WebDriver driver,final TimeUnit timeUnit,final long retryInterval,long timeOut) {
		boolean notDisplay = false;
		Wait<WebDriver> wait = new WebDriverWait(driver, TimeUnit.SECONDS.convert(timeOut, timeUnit));
		try{
			notDisplay =  wait.until(new ExpectedCondition<Boolean>() {
				boolean notDisplay = true;
				public Boolean apply(WebDriver input) {
					try {
						notDisplay = Boolean.valueOf(((JavascriptExecutor) input).executeScript("return (window.angular !== undefined) && (angular.element(document).injector() !== undefined) && (angular.element(document).injector().get('$http').pendingRequests.length === 0)").toString());
						if (!notDisplay) {
							Log.info("Angular Displayed On Screen.");
						} else {
							Log.info("Angular Not Displayed On Screen.");
							return true;
						}
						try {
							Thread.sleep(TimeUnit.MILLISECONDS.convert(retryInterval, timeUnit));
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					} catch (RuntimeException e) {
						notDisplay = true;
						Log.info(" Angular Not Displayed On Screen."+ e);
						try {
							Thread.sleep(TimeUnit.MILLISECONDS.convert(retryInterval, timeUnit));
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}
					return notDisplay;
				}
			});
		}
		catch(Exception e){
			Log.error("Angular Not invisible with in " + TimeUnit.SECONDS.convert(timeOut, timeUnit) + " Seconds");
			throw e;
		}
		return notDisplay;
	}

	public void waitForLoad() throws Throwable{
		try {
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.AJAX_MAX_EXPLICIT_WAIT_TIME_OUT);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//span[@class='loading']")));
			Thread.sleep(4000);
		} catch (TimeoutException e) {
			Log.error("Not Loaded." + e);
			throw new TimeoutException(e);
		}
	}

	public void waitForLoad_Long() throws Throwable{
		try {
			Thread.sleep(4000);
			Wait<WebDriver> wait = new WebDriverWait(driver, TestAutomationConstants.AJAX_MAX_EXPLICIT_WAIT_TIME_OUT);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//span[@class='loading']")));
			Thread.sleep(4000);
		} catch (TimeoutException e) {
			Log.error("Not Loaded." + e);
			throw new TimeoutException(e);
		}
	}

	public String getPageTitle(){
		return driver.getTitle().trim();
	}

	//Wait for JQuery Load
	public void waitForJQueryLoad(WebDriver driver) {
		//Wait for jQuery to load

		WebDriverWait jsWait = new WebDriverWait(driver,15);
		ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return ((Long) ((JavascriptExecutor) driver)
						.executeScript("return jQuery.active") == 0);
			}
		};

		//Get JQuery is Ready
		boolean jqueryReady = (Boolean) ((JavascriptExecutor)driver).executeScript("return jQuery.active==0");

		//Wait JQuery until it is Ready!
		if(!jqueryReady) {
			System.out.println("JQuery is NOT Ready!");
			//Wait for jQuery to load
			jsWait.until(jQueryLoad);
		} else {
			System.out.println("JQuery is Ready!");
		}
	}

	//Wait for Angular Load
	public void waitForAngularLoad(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver,15);
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		final String angularReadyScript = "return angular.element(document).injector().get('$http').pendingRequests.length === 0";

		//Wait for ANGULAR to load
		ExpectedCondition<Boolean> angularLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return Boolean.valueOf(((JavascriptExecutor) driver)
						.executeScript(angularReadyScript).toString());
			}
		};

		//Get Angular is Ready
		boolean angularReady = Boolean.valueOf(jsExec.executeScript(angularReadyScript).toString());

		//Wait ANGULAR until it is Ready!
		if(!angularReady) {
			System.out.println("ANGULAR is NOT Ready!");
			//Wait for Angular to load
			wait.until(angularLoad);
		} else {
			System.out.println("ANGULAR is Ready!");
		}
	}

	//Wait Until JS Ready
	public void waitUntilJSReady(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver,15);
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		//Wait for Javascript to load
		ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver)
						.executeScript("return document.readyState").toString().equals("complete");
			}
		};

		//Get JS is Ready
		boolean jsReady =  (Boolean) jsExec.executeScript("return document.readyState").toString().equals("complete");

		//Wait Javascript until it is Ready!
		if(!jsReady) {
			System.out.println("JS in NOT Ready!");
			//Wait for Javascript to load
			wait.until(jsLoad);
		} else {
			System.out.println("JS is Ready!");
		}
	}

	//Wait Until JQuery and JS Ready
	public void waitUntilJQueryReady(WebDriver driver) {
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		//First check that JQuery is defined on the page. If it is, then wait AJAX
		Boolean jQueryDefined = (Boolean) jsExec.executeScript("return typeof jQuery != 'undefined'");
		if (jQueryDefined == true) {
			//Pre Wait for stability (Optional)
			sleep(20);

			//Wait JQuery Load
			waitForJQueryLoad(driver);

			//Wait JS Load
			waitUntilJSReady(driver);

			//Post Wait for stability (Optional)
			sleep(20);
		}  else {
			System.out.println("jQuery is not defined on this site!");
		}
	}

	//Wait Until Angular and JS Ready
	public void waitUntilAngularReady(WebDriver driver) {
		JavascriptExecutor jsExec = (JavascriptExecutor) driver;

		//First check that ANGULAR is defined on the page. If it is, then wait ANGULAR
		Boolean angularUnDefined = (Boolean) jsExec.executeScript("return window.angular === undefined");
		if (!angularUnDefined) {
			Boolean angularInjectorUnDefined = (Boolean) jsExec.executeScript("return angular.element(document).injector() === undefined");
			if(!angularInjectorUnDefined) {
				//Pre Wait for stability (Optional)
				sleep(20);

				//Wait Angular Load
				waitForAngularLoad(driver);

				//Wait JS Load
				waitUntilJSReady(driver);

				//Post Wait for stability (Optional)
				sleep(20);
			} else {
				System.out.println("Angular injector is not defined on this site!");
			}
		}  else {
			System.out.println("Angular is not defined on this site!");
		}
	}

	//Wait Until JQuery Angular and JS is ready
	public void waitJQueryAngular(WebDriver driver) {
		waitUntilJQueryReady(driver);
		waitUntilAngularReady(driver);
	}

	public static void sleep (long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void writeFile(String text,String filePath,boolean createNew) {
		try {

			File file = new File(filePath);

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile(), createNew);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(text);
			bw.write("\r\n");
			bw.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String fileReadLastNthLine( String filePath, int lines) {
		RandomAccessFile randomAccessFile = null;
		try {
			randomAccessFile = new RandomAccessFile( filePath, "r" );
			long fileLength = randomAccessFile.length() - 1;
			StringBuilder sb = new StringBuilder();
			int line = 0;

			for(long filePointer = fileLength; filePointer != -1; filePointer--){
				randomAccessFile.seek( filePointer );
				char readChar = (char) randomAccessFile.read();

				if( readChar == '\n' ) {
					if (filePointer < fileLength) {
						line = line + 1;
					}
				} else if( readChar == '\r' ) {
					if (filePointer < fileLength-1) {
						line = line + 1;
					}
				}
				if (line >= lines) {
					break;
				}
				sb.append( readChar );
			}

			String lastLine = sb.reverse().toString();
			return lastLine;
		} catch( java.io.FileNotFoundException e ) {
			e.printStackTrace();
			return null;
		} catch( java.io.IOException e ) {
			e.printStackTrace();
			return null;
		}
		finally {
			if (randomAccessFile != null )
				try {
					randomAccessFile.close();
				} catch (IOException e) {
				}
		}
	}

	public Gson getJSONObject() throws Throwable{
		Gson gson = null;
		try {
			GsonBuilder builder = new GsonBuilder();
			builder.excludeFieldsWithoutExposeAnnotation();
			builder.setPrettyPrinting();
			gson = builder.create();	
		} catch (Exception e) {
			Log.error("Error Happened To Create 'GSON' object. " + e);
			throw new Throwable("Error Happened To Create 'GSON' object. ");
		}
		return gson;
	}

	public void takeScreenShot(WebDriver driver, String screenShotName) throws Exception {
		WebDriver augmentedDriver = new Augmenter().augment(driver);
		File screenshot = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy_hh.mm.ss");
		Date date = new Date();
		String modifiedScreenShotName = screenShotName + "_"+ dateFormat.format(date) + ".png";
		String path = new File("target/surefire-reports/screen-shots/" + modifiedScreenShotName).getAbsolutePath();
		System.out.println(path);		
		FileUtils.copyFile(screenshot, new File(path));
		//		Reporter.log("<a href= file:///" + path + "</a>");
		//			Reporter.log("<a href= file:///"+ path +"> <img src='"+ path + "' height='100' width='100'/> </a>");
	} 

	public byte[] takeScreenShot(WebDriver driver) throws Exception {
		WebDriver augmentedDriver = new Augmenter().augment(driver);
		return ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.BYTES);
	}

	public void scrollIntoView(WebElement element){
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
	
	public String extractStringRegEx(String actualString, String pattern, int groupNumber) {
		String extractedString = "";
		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		// Now create matcher object.
		Matcher m = r.matcher(actualString);
		if (m.find( )) {
			extractedString = m.group(groupNumber);
			Log.debug("Found value: " + extractedString);
		}
		return extractedString;
	}
}