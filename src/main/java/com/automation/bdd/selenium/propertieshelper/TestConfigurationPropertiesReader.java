package com.automation.bdd.selenium.propertieshelper;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This is a Singleton class to make sure only one time it will be loaded.
 */
public class TestConfigurationPropertiesReader {

	private static Logger Log = LogManager.getLogger(TestConfigurationPropertiesReader.class);
	
	private static TestConfigurationPropertiesReader propfile;
	private Properties properties;

	public static TestConfigurationPropertiesReader getInstance() throws Throwable {
		if (propfile == null) {
			synchronized (TestConfigurationPropertiesReader.class) {
				if (propfile == null) {
					propfile = new TestConfigurationPropertiesReader();
				}
			}
		}
		return propfile;
	}

	private TestConfigurationPropertiesReader() throws Throwable {
		try {
			File file = new File("./src/test/resources/config/test-config.properties");
			FileInputStream fileInputStream = new FileInputStream(file);
			properties = new Properties();
			properties.load(fileInputStream);
			fileInputStream.close();

		} catch (Exception e) {
			Log.error("Error Happened To Load Test Configuration Properties File. " + e);
			throw new Throwable("Error Happened To Load Test Configuration Properties File. ");
		}
	}

	public String getProperty(String property){
		return properties.getProperty(property).trim();
	}
}
