package com.automation.bdd.selenium.driver;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.automation.bdd.selenium.helper.TestAutomationConstants;
import com.automation.bdd.selenium.propertieshelper.TestConfigurationPropertiesReader;
import com.google.common.base.Strings;

import lombok.Data;

@Data
public class TestConfigurationSetUp {

	private static final Logger Log = LogManager.getLogger(TestConfigurationSetUp.class);

	private static TestConfigurationSetUp testConfigSetUp;
	private String browser;
	private boolean isRemote;
	private String hubURL;
	private String applicationURL;

	private TestConfigurationSetUp(){

	}

	public static TestConfigurationSetUp getInstance() {
		if (testConfigSetUp == null) {
			synchronized (TestConfigurationSetUp.class) {
				if (testConfigSetUp == null) {
					testConfigSetUp = new TestConfigurationSetUp();
				}
			}
		}
		return testConfigSetUp;
	}

	/**
	 * @param testConfigurationProperties object hold the all value from test-config.properties file
	 * 
	 * This method created for load and assign the value of Test Configuration related values and 
	 * TestautomationConstant values.
	 * @throws Throwable 
	 */
	public void loadTestConfig(TestConfigurationPropertiesReader testConfigurationProperties) throws Throwable{

		TestAutomationConstants.AJAX_MAX_EXPLICIT_WAIT_TIME_OUT = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("AJAXMAXIMUMWAITTIMEINSECONDS")) == true
				? TestAutomationConstants.AJAX_MAX_EXPLICIT_WAIT_TIME_OUT : Integer.parseInt(testConfigurationProperties.getProperty("AJAXMAXIMUMWAITTIMEINSECONDS"));

		TestAutomationConstants.AJAX_AVG_EXPLICIT_WAIT_TIME_OUT = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("AJAXAVGWAITTIMEINSECONDS")) == true 
				? TestAutomationConstants.AJAX_AVG_EXPLICIT_WAIT_TIME_OUT : Integer.parseInt(testConfigurationProperties.getProperty("AJAXAVGWAITTIMEINSECONDS"));

		TestAutomationConstants.AJAX_MIN_EXPLICIT_WAIT_TIME_OUT = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("AJAXMINIMUMWAITTIMEINSECONDS")) == true
				? TestAutomationConstants.AJAX_MIN_EXPLICIT_WAIT_TIME_OUT : Integer.parseInt(testConfigurationProperties.getProperty("AJAXMINIMUMWAITTIMEINSECONDS"));

		TestAutomationConstants.TIMEOUT_MAX = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("MAXIMUMWAITTIMEINSECONDS")) == true 
				? TestAutomationConstants.TIMEOUT_MAX : Integer.parseInt(testConfigurationProperties.getProperty("MAXIMUMWAITTIMEINSECONDS"));

		TestAutomationConstants.TIMEOUT_AVG = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("AVGWAITTIMEINSECONDS")) == true
				? TestAutomationConstants.TIMEOUT_AVG : Integer.parseInt(testConfigurationProperties.getProperty("AVGWAITTIMEINSECONDS"));

		TestAutomationConstants.TIMEOUT_MIN = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("MINIMUMWAITTIMEINSECONDS")) == true
				? TestAutomationConstants.TIMEOUT_MIN : Integer.parseInt(testConfigurationProperties.getProperty("MINIMUMWAITTIMEINSECONDS"));

		TestAutomationConstants.POLLING = Strings.isNullOrEmpty(testConfigurationProperties.getProperty("AJAXPOLLINGTIMEINSECONDS")) == true
				? TestAutomationConstants.POLLING : Integer.parseInt(testConfigurationProperties.getProperty("AJAXPOLLINGTIMEINSECONDS"));

		String browserNameFromRunTime= System.getProperty("browser");
		Log.info("Browser Name Set From Runtime As :" + browserNameFromRunTime);
		String remoteFromRunTime= System.getProperty("remote");
		Log.info("Remote Set From Runtime As :" + remoteFromRunTime);
		String hubURLFromRunTime= System.getProperty("hub");
		Log.info("HUB URL Set From Runtime As :" + hubURLFromRunTime);

		browser = Strings.isNullOrEmpty(browserNameFromRunTime) ? testConfigurationProperties.getProperty("BROWSER") : browserNameFromRunTime;
		Log.info("Browser Name Set As :" + browser);
		isRemote = Strings.isNullOrEmpty(remoteFromRunTime) ? (Strings.isNullOrEmpty(testConfigurationProperties.getProperty("REMOTEDRIVER")) 
				? false : StringUtils.containsIgnoreCase(testConfigurationProperties.getProperty("REMOTEDRIVER"),"false") ? false : true) 
				: (StringUtils.containsIgnoreCase(remoteFromRunTime, "false") ? false : true);
		Log.info("RemoteSet As :" + isRemote);

		if(isRemote){
			hubURL = Strings.isNullOrEmpty(hubURLFromRunTime) ? testConfigurationProperties.getProperty("HUBURL") : hubURLFromRunTime;
			Log.info("HUB URL Set As :" + hubURL);
			if (Strings.isNullOrEmpty(hubURL)) {
				throw new Throwable("Remote Run Need Hub URL HUB URL Can't Be Null Or Empty. ");
			}
		}

		applicationURL = testConfigurationProperties.getProperty("URL");
	}
}
