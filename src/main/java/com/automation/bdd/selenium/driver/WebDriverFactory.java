package com.automation.bdd.selenium.driver;

import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;


/**
 * @author ArjunRay
 *
 */
public class WebDriverFactory {

	private static Logger Log = LogManager.getLogger(WebDriverFactory.class);

	private static ThreadLocal<WebDriver> threadWebDriver = new ThreadLocal<WebDriver>();
	public static WebDriver getThreadedWebDriver(BrowserType browserTypeEnum, TestConfigurationSetUp testConfig) throws IOException {
		WebDriver currentDriver = threadWebDriver.get();
		/**
		 * Get the driver currently used for the thread.
		 * Lazy loaded, if there is not driver yet, create it.
		 */

		if (currentDriver == null)
		{
			if(testConfig.isRemote()){
				Log.debug("Creating remote web driver instance for "+browserTypeEnum);
				try {
					currentDriver = SeleniumRemoteWebDriverFactory.getWebDriver(browserTypeEnum, testConfig);
				} catch (MalformedURLException e) {
					throw new RuntimeException("Error occured while instantiating remote web driver for "+browserTypeEnum,e);
				}
			}else{
				Log.info("Creating local web driver instance for "+browserTypeEnum);
				currentDriver = SeleniumLocalWebDriverFactory.getWebDriver(browserTypeEnum, testConfig);
			}
			threadWebDriver.set(currentDriver);
		}
		return currentDriver;
	}

	public static void removeThreadedDriver() {
		threadWebDriver.remove();
	}
}