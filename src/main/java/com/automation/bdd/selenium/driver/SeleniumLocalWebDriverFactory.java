package com.automation.bdd.selenium.driver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import io.github.bonigarcia.wdm.config.DriverManagerType;


public class SeleniumLocalWebDriverFactory {

	private static final Logger Log = LogManager.getLogger(SeleniumLocalWebDriverFactory.class);

	/**
	 * @param Browser Type Enum
	 * @return WebDriver Instance as per Browser type
	 */
	public static WebDriver getWebDriver(BrowserType browser, TestConfigurationSetUp testConfig){
		WebDriver driver=null;
		DesiredCapabilities capabilities = getDesiredCapabilities(browser);
		switch(browser){
		case CHROME:
			WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); 
			ChromeOptions chromeOption = new ChromeOptions();
			chromeOption.merge(capabilities);
			driver = new ChromeDriver(chromeOption);
			break;
		case CHROMEHEADLESS:
			WebDriverManager.getInstance(DriverManagerType.CHROME).setup(); 
			ChromeOptions chromeHeadLessOption = new ChromeOptions();
			chromeHeadLessOption.merge(capabilities);
			driver = new ChromeDriver(chromeHeadLessOption);
			break;
		case FIREFOX:
			WebDriverManager.getInstance(DriverManagerType.FIREFOX).setup();
			FirefoxOptions fireFoxOption = new FirefoxOptions();
			fireFoxOption.merge(capabilities);
			driver = new FirefoxDriver(fireFoxOption);
			break;	
		case IE:
			WebDriverManager.getInstance(DriverManagerType.IEXPLORER).setup();
			InternetExplorerOptions internetExplorerOptions = new InternetExplorerOptions();
			internetExplorerOptions.merge(capabilities);
			driver = new InternetExplorerDriver(internetExplorerOptions);
			break;
		default:
			throw new RuntimeException("Invalid BrowserType passed");
		}
		return driver;		
	}


	/**
	 * Helper method to call appropriate methods on DesiredCapabilities object.  
	 * This will likely need to be updated as the tests want
	 * to call on more specific capabilities.
	 */
	private static DesiredCapabilities getDesiredCapabilities(BrowserType browserTypeEnum){
		DesiredCapabilities capabilities = null;	        

		switch (browserTypeEnum) {
		case IE:
			capabilities = InternetExplorerCapabilities();
			break;
		case CHROME:
			capabilities = ChromeCapabilities();
			break;
		case CHROMEHEADLESS:
			capabilities = ChromeHeadLessCapabilities();
			break;	
		case FIREFOX:
			capabilities = FirefoxCapabilities();
			break;
		default:
			throw new RuntimeException("Invalid BrowserType passed");
		}

		return capabilities;
	}


	/**
	 * @return Firefox Capability instance
	 */
	private static DesiredCapabilities FirefoxCapabilities() {
		DesiredCapabilities capabilities = null;
		try {
			//			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"/src/test/resources/drivers/geckodriver_win32/geckodriver.exe");
			capabilities = DesiredCapabilities.firefox();
			capabilities.setAcceptInsecureCerts(true);
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability(FirefoxDriver.MARIONETTE, true);
		} catch (WebDriverException wbe) {
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;
	}


	/**
	 * @return InternetExplorer Capability instance
	 */
	private static DesiredCapabilities InternetExplorerCapabilities() {
		DesiredCapabilities capabilities = null;
		try{
			//			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"/src/test/resources/drivers/IEDriverServer_Win32/IEDriverServer.exe");
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
		}catch(WebDriverException wbe){
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;		
	}


	/**
	 * @return Chrome Capability instance
	 */
	private static DesiredCapabilities ChromeCapabilities() {
		DesiredCapabilities capabilities = null;
		try{
			//			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/src/test/resources/drivers/chromedriver_win32/chromedriver.exe" );
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--allow-running-insecure-content");
			options.addArguments("--allow-insecure-websocket-from-https-origin");
			options.addArguments("disable-extensions");
			capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}catch(WebDriverException wbe){
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;
	}

	/**
	 * @return Chrome HeadLess Capability instance
	 */
	private static DesiredCapabilities ChromeHeadLessCapabilities() {
		DesiredCapabilities capabilities = null;
		try{
			//			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/src/test/resources/drivers/chromedriver_win32/chromedriver.exe" );
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--allow-running-insecure-content");
			options.addArguments("--allow-insecure-websocket-from-https-origin");
			options.addArguments("disable-extensions");
			options.addArguments("--disable-plugins");
			options.addArguments("--headless", "--disable-gpu", "--no-sandbox", "--incognito", "window-size=1920,1200" );
			capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}catch(WebDriverException wbe){
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;
	}
}