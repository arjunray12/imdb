package com.automation.bdd.selenium.driver;

/**
 * 
 * ENUM class containing the name of all possible browsers applicable for
 * automation test.
 *
 */
public enum BrowserType {
	FIREFOX, IE, CHROME, CHROMEHEADLESS ;
}

