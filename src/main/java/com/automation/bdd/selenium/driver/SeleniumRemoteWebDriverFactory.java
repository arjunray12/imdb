package com.automation.bdd.selenium.driver;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.google.common.base.Strings;

public class SeleniumRemoteWebDriverFactory {

	private static final Logger Log = LogManager.getLogger(SeleniumRemoteWebDriverFactory.class);

	/**
	 * Create Web Driver based on the browser type passed as the input parameter.
	 * 
	 * @param browserTypeEnum - the browser type
	 * 
	 * @return the Web Driver instance
	 */
	public static WebDriver getWebDriver(BrowserType browserTypeEnum, TestConfigurationSetUp testconfig) throws MalformedURLException{

		Log.debug("Get Remote Web Driver for " + browserTypeEnum);
		Capabilities capabilities = getDesiredCapabilities(browserTypeEnum);		
		String hubURL = testconfig.getHubURL();
		Log.info("Hub URL is : "+hubURL);
		if(Strings.isNullOrEmpty(hubURL)){
			throw new RuntimeException("Hub URL is null or empty");
		}
		WebDriver webDriver = new RemoteWebDriver(new URL(hubURL), capabilities);
		Log.debug("Going to maximize Remote Web Browser for "+browserTypeEnum);
		return webDriver;
	}

	/**
	 * Helper method to call appropriate methods on DesiredCapabilities object.  
	 * This will likely need to be updated as the tests want
	 * to call on more specific capabilities.
	 */
	private static DesiredCapabilities getDesiredCapabilities(BrowserType browserTypeEnum){
		DesiredCapabilities capabilities = null;	        

		switch (browserTypeEnum) {
		case IE:
			capabilities = InternetExplorerCapabilities();
			break;
		case CHROME:
			capabilities = ChromeCapabilities();
			break;
		case CHROMEHEADLESS:
			capabilities = ChromeHeadLessCapabilities();
			break;
		case FIREFOX:
			capabilities = FirefoxCapabilities();
			break;
		default:
			throw new RuntimeException("Invalid BrowserType passed");
		}

		return capabilities;
	}


	/**
	 * @return Firefox Capability instance
	 */
	private static DesiredCapabilities FirefoxCapabilities() {
		DesiredCapabilities capabilities = null;
		try {
			capabilities = DesiredCapabilities.firefox();
			capabilities.setAcceptInsecureCerts(true);
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability(FirefoxDriver.MARIONETTE, true);
		} catch (WebDriverException wbe) {
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;
	}


	/**
	 * @return InternetExplorer Capability instance
	 */
	private static DesiredCapabilities InternetExplorerCapabilities() {
		DesiredCapabilities capabilities = null;
		try{
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability("ignoreProtectedModeSettings", true);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
		}catch(WebDriverException wbe){
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;		
	}


	/**
	 * @return Chrome Capability instance
	 */
	private static DesiredCapabilities ChromeCapabilities() {
		DesiredCapabilities capabilities = null;
		try{
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--allow-running-insecure-content");
			options.addArguments("--allow-insecure-websocket-from-https-origin");
			options.addArguments("disable-extensions");
			capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}catch(WebDriverException wbe){
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;
	}

	/**
	 * @return Chrome HeadLess Capability instance
	 */
	private static DesiredCapabilities ChromeHeadLessCapabilities() {
		DesiredCapabilities capabilities = null;
		try{
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--allow-running-insecure-content");
			options.addArguments("--allow-insecure-websocket-from-https-origin");
			options.addArguments("disable-extensions");
			options.addArguments("--disable-plugins");
			options.addArguments("--headless", "--disable-gpu", "--no-sandbox", "--incognito", "window-size=1920,1200" );
			capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		}catch(WebDriverException wbe){
			Log.error("Error: " + wbe.getMessage());
		}
		return capabilities;
	}
}